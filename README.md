

---

# A Gitbook Reference for Advanced PowerShell

---

The following gitbook is a reference for using PowerShell at an advanced level. The focus of the materila will include automating, pentesting, securing, data integration, cryptography and other general tooling in PowerShell. The reference is not to teach you PowerShell basics, but to deliver methods of use for PowerShell. 
